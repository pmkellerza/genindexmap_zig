const std = @import("std");
pub const GenerationalIndex = @import("main.zig").GenIndex.GenerationalIndex;

fn ArrayEntry(comptime T: type) type {
    return struct {
        const Self = @This();

        component: T,
        generation: usize,
    };
}

pub fn GenerationIndexArray(comptime T: type) type {
    return struct {
        const Self = @This();

        list: std.ArrayList(?ArrayEntry(T)),

        pub fn init(alloc: std.mem.Allocator) Self {
            return Self{
                .list = std.ArrayList(?ArrayEntry(T)).init(alloc),
            };
        }

        pub fn deinit(self: *Self) void {
            self.list.clearAndFree();
            self.list.deinit();

            self.list = undefined;
        }

        pub fn insert(self: *Self, gen_index: GenerationalIndex, component: T) !void {
            while (gen_index.index >= self.list.items.len) {
                try self.list.append(null);
            }

            self.list.items[gen_index.index] = ArrayEntry(T){
                .component = component,
                .generation = gen_index.generation,
            };
        }

        pub fn get_component(self: *Self, gen_index: GenerationalIndex) ?T {
            if (self.list.items[gen_index.index]) |entry| {
				return entry.component;
            } else {
                return null;
            }
        }

        pub fn drop(self: *Self, gen_index: GenerationalIndex) void {
            if (self.list.items[gen_index.index]) |item| {
                if (item.generation == gen_index.generation) {
                    self.list.items[gen_index.index] = null;
                }
            }
        }
    };
}
