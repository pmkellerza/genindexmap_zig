const std = @import("std");

// -------- GenerationalIndex Section
pub const GenerationalIndex = struct {
    index: usize,
    generation: u64,

    const Self = @This();
};

pub const AllocatorEntry = struct {
    active: bool,
    generation: u64,
};

pub const GenerationalIndexAllocator = struct {
    entries: std.ArrayList(AllocatorEntry),
    available: std.ArrayList(usize),

    const Self = @This();
    pub fn new(alloc: std.mem.Allocator) !Self {
        return Self{
            .entries = std.ArrayList(AllocatorEntry).init(alloc),
            .available = std.ArrayList(usize).init(alloc),
        };
    }

    pub fn allocate(self: *Self) !GenerationalIndex {
        const maybeValue: ?usize = self.available.getLastOrNull();

        if (maybeValue) |value| {
            var id_entry: AllocatorEntry = self.entries.items[value];

            id_entry.active = true;
            id_entry.generation += 1;

            return GenerationalIndex{ .index = value, .generation = id_entry.generation };
        } else {
            try self.entries.append(AllocatorEntry{ .generation = 0, .active = true });

            return GenerationalIndex{ .index = self.entries.items.len - 1, .generation = 0 };
        }
    }

    pub fn deallocate(self: *Self, gen_index: GenerationalIndex) !void {
        var someEntry: ?AllocatorEntry = self.entries.items[gen_index.index];
        if (someEntry) |*entry| {
            if (entry.active and entry.generation == gen_index.generation) {
                entry.active = false;
                try self.available.append(gen_index.index);
            }
        }
    }

    pub fn deinit(self: *Self) void {
        self.available.clearAndFree();
        self.available.deinit();

        self.entries.clearAndFree();
        self.entries.deinit();

        self.entries = undefined;
        self.available = undefined;
    }
};
